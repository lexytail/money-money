var find = function (selector) { return document.querySelector(selector); };
var find_all = function (selector) { return document.querySelectorAll(selector); };
var random = function (max, min) {
    if (min === void 0) { min = 0; }
    return (Math.random() * (max - min) + min) | 0;
};
var $body = document.body;
var ls = localStorage;
var $popups = find('.popups');
var $estimate_popup = find("#estimate-popup");
var $sell_popup = find("#sell-popup");
var $withdrawal_popup = find("#withdrawal-popup");
var $open_buttons = find_all('[open-estimate]');
var $estimate_finish = find('#estimate-finish');
// Estimate module
!function () {
    var $wait_counter = find('#estimate-counter');
    var $estimate_buyers = find('#estimate-buyers');
    var $estimate_income = find('#estimate-income');
    $open_buttons.forEach(function ($element) { return $element.addEventListener('click', run_estimate); });
    function run_estimate() {
        var wait_time = 60;
        show($popups);
        down_time();
        var counter = setInterval(function () {
            down_time();
            if (wait_time <= 55) {
                clearInterval(counter);
                hide($wait_counter);
                show($estimate_finish);
                $estimate_buyers.classList.add('bold');
                $estimate_buyers.innerText = random(800000, 200000) + " \u0441\u0430\u0439\u0442\u0430";
                $estimate_income.className += ' green bold';
                $estimate_income.innerText = random(40000, 35000) + " \u0420\u0443\u0431";
            }
        }, 1000);
        function down_time() {
            $wait_counter.innerText = "\u0414\u043E \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043D\u0438\u044F \u043E\u0441\u0442\u0430\u043B\u043E\u0441\u044C: " + wait_time + " \u0441\u0435\u043A\u0443\u043D\u0434";
            wait_time--;
        }
    }
}();
// Sell module
!function () {
    var $wait_counter = find('#sell-counter');
    $estimate_finish.addEventListener('click', run_sell);
    function run_sell() {
        hide($estimate_popup);
        show($sell_popup);
        var wait_time = 60;
        down_time();
        var counter = setInterval(function () {
            down_time();
            if (wait_time <= 55) {
                clearInterval(counter);
            }
        }, 1000);
        function down_time() {
            $wait_counter.innerText = "\u0414\u043E \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043D\u0438\u044F \u043E\u0441\u0442\u0430\u043B\u043E\u0441\u044C: " + wait_time + " \u0441\u0435\u043A\u0443\u043D\u0434";
            wait_time--;
        }
    }
}();
function hide($element) {
    $element.classList.add('hidden');
}
function show($element) {
    $element.classList.remove('hidden');
}
