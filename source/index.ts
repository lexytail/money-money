const find = (selector: string): HTMLElement => document.querySelector(selector)

const find_all = (selector: string): NodeListOf<HTMLElement> => document.querySelectorAll(selector)

const random = (max: number, min: number = 0): number => (Math.random() * (max - min) + min) | 0

const $body: HTMLBodyElement = <HTMLBodyElement>document.body

const ls: Storage = localStorage

const $popups: HTMLElement = find('.popups')

const $estimate_popup: HTMLElement = find("#estimate-popup")

const $sell_popup: HTMLElement = find("#sell-popup")

const $withdrawal_popup: HTMLElement = find("#withdrawal-popup")

const $open_buttons: NodeListOf<HTMLElement> = find_all('[open-estimate]')

const $estimate_finish: HTMLElement = find('#estimate-finish')

// Estimate module
!function () {

  const $wait_counter: HTMLElement = find('#estimate-counter')
  const $estimate_buyers: HTMLElement = find('#estimate-buyers')
  const $estimate_income: HTMLElement = find('#estimate-income')

  $open_buttons.forEach(($element: HTMLElement) => $element.addEventListener('click', run_estimate))

  function run_estimate(): void {

    let wait_time: number = 60

    show($popups)

    down_time()

    let counter: number = setInterval(() => {

      down_time()

      if (wait_time <= 55) {

        clearInterval(counter)

        hide($wait_counter)

        show($estimate_finish)

        $estimate_buyers.classList.add('bold')
        $estimate_buyers.innerText = `${random(800000, 200000)} сайта`

        $estimate_income.className += ' green bold'
        $estimate_income.innerText = `${random(40000, 35000)} Руб`

      }

    }, 1000)

    function down_time(): void {

      $wait_counter.innerText = `До завершения осталось: ${wait_time} секунд`

      wait_time--

    }

  }



}()

// Sell module
!function () {

  const $wait_counter: HTMLElement = find('#sell-counter')

  $estimate_finish.addEventListener('click', run_sell)

  function run_sell(): void {

    hide($estimate_popup)

    show($sell_popup)

    let wait_time: number = 60

    down_time()

    let counter: number = setInterval(() => {

      down_time()

      if (wait_time <= 55) {

        clearInterval(counter)

      }

    }, 1000)

    function down_time(): void {

      $wait_counter.innerText = `До завершения осталось: ${wait_time} секунд`

      wait_time--

    }

  }

}()



function hide($element: HTMLElement): void {

  $element.classList.add('hidden')

}

function show($element: HTMLElement): void {

  $element.classList.remove('hidden')

}