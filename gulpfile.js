const gulp = require('gulp'), sass = require('gulp-sass'), pug = require('gulp-pug'), ts = require('gulp-typescript')

gulp.task('view', () =>

	gulp.src('./source/*.pug')
		.pipe(pug())
		.pipe(gulp.dest('./dist'))

)

gulp.task('style', () => 

	gulp.src('./source/*.sass')
		.pipe(sass())
		.pipe(gulp.dest('./dist'))

)

gulp.task('script', () => 

	gulp.src('./source/*.ts')
		.pipe(ts())
		.pipe(gulp.dest('./dist'))

)

gulp.task('assets', () => 

	gulp.src('./source/assets/**/*')
		.pipe(gulp.dest('./dist'))

)

gulp.task('watch', () => {

	gulp.watch('./source/**/*.pug', () => gulp.start('view'))
	gulp.watch('./source/**/*.sass', () => gulp.start('style'))

})

gulp.task('default', ['view', 'style', 'script', 'assets'])